tomcat5.5 (5.5.26-5) unstable; urgency=medium

  * Merge changes from Ubuntu:
    - Use default-jre-headless, default-jdk as preferred alternatives.
    - tomcat5.5.init: Fix JDK list to match default-jre, java-6-openjdk
      and java-6-cacao. Closes: #495235.
    - tomcat5.5.postinst: Removed superfluous /etc/tomcat5.5/tomcat5.5 linking.
      Closes: #498487.
  * debian/copyright: Reference Apache 2.0 license in /usr/share/common/licenses

 -- Matthias Klose <doko@debian.org>  Wed, 15 Oct 2008 14:00:39 +0200

tomcat5.5 (5.5.26-4) unstable; urgency=high

  * Security issues fixed.
    - CVE-2008-1232: Cross-site scripting
    - CVE-2008-2370: Information disclosure
    - CVE-2008-2938: Directory traversal. Closes: #496309.

 -- Marcus Better <marcus@better.se>  Sun, 05 Oct 2008 14:15:19 +0200

tomcat5.5 (5.5.26-3) unstable; urgency=high

  * CVE-2008-1947: Fix XSS issue in host-manager web application.
    Closes: #484643

 -- Michael Koch <konqueror@gmx.de>  Fri, 06 Jun 2008 09:34:15 +0200

tomcat5.5 (5.5.26-2) unstable; urgency=low

  * Fixed bootstrap.MF to put commons-logging-api.jar onto classpath instead
    of commons-logging-api-1.1.1.jar. Closes: #477363

 -- Michael Koch <konqueror@gmx.de>  Sun, 01 Jun 2008 11:34:40 +0200

tomcat5.5 (5.5.26-1) unstable; urgency=low

  [ Michael Koch ]
  * New upstream release.
    - CVE-2007-5333: unauthorized disclosure of information. Closes: #465645
    - CVS-2007-6286: handling of empty requests.
  * debian/rules: Don't file when files to delete don't exist.
    Closes: #458977
  * debian/tomcat5.5.init: Change directory to $CATALINA_BASE/temp before
    starting the daemon. Patch by David Pashley. Closes: #418826
  * debian/tomcat5.5.init: Use 'printf' instead of 'echo -e'.
    Closes: #472899

  [ Marcus Better ]
  * debian/policy/04webapps.policy: Grant read permission to JULI for the
    (non-existing) logging.properties file in the example webapps. Closes:
    #460839.

 -- Michael Koch <konqueror@gmx.de>  Sat, 19 Apr 2008 23:18:30 +0200

tomcat5.5 (5.5.25-5) unstable; urgency=low

  * debian/tomcat5.5.init: Check if tomcat-users.xml exists.
    Thanks to Javier Serrano Polo for the patch. Closes: #445857.
  * debian/tomcat5.5-webapps.postrm: Purge links created in postinst script.
    Closes: #453879.
  * debian/tomcat5.5-admin.links: Fix symlink for commons-io.jar.
    Closes: #452366.
  * debian/tomcat5.5.init: Check user id of the user running the init script.
    Closes: #457956.
  * Renamed /etc/cron.daily/tomcat5.5 to /etc/cron.daily/tomcat55.
    Closes: #454296.
  * debian/tomcat5.5.init: source /etc/default/locale and export LANG so
    tomcat gets started with system locale. Originally reported to
    https://bugs.launchpad.net/ubuntu/+source/tomcat5.5/+bug/153672.

 -- Michael Koch <konqueror@gmx.de>  Thu, 03 Jan 2008 13:23:44 +0100

tomcat5.5 (5.5.25-4) unstable; urgency=high

  * CVE-2007-5342: Fix unauthorized modification of data because of
    too open permissions. Closes: #458237.
  * Always clean temporary directory on startup. Closes: #456608.

 -- Michael Koch <konqueror@gmx.de>  Sat, 29 Dec 2007 20:15:40 +0100

tomcat5.5 (5.5.25-3) unstable; urgency=low

  * debian/libtomcat5.5-java.links: Removed links for xml-apis.jar and
    xercesImpl.jar. Closes: #443382, #455495.
  * Added libgnumail-java to Build-Depends. Closes: #454312.
  * Updated Standards-Version to 3.7.3.

 -- Michael Koch <konqueror@gmx.de>  Thu, 13 Dec 2007 22:15:18 +0100

tomcat5.5 (5.5.25-2) unstable; urgency=high

  [ Michael Koch ]
  CVE-2007-5461:
  * Fix absolute path traversal vulnerability. Closes: #448664.

  [ Marcus Better ]
  * Add required commons-io symlink to the admin webapp, which fixes WAR
    file uploads. (Closes: #452366)
  * debian/control: Use the new Homepage and Vcs-* fields.
  * debian/NEWS: Remove outdated entry.

 -- Michael Koch <konqueror@gmx.de>  Fri, 30 Nov 2007 10:46:33 +0100

tomcat5.5 (5.5.25-1) unstable; urgency=high

  * New upstream release. Setting usrgency to high.
    - Fixes XSS issues. CVE-2007-1355, CVS-2007-2449 and CVE-2007-2450.
  * debian/policy/04webapps.policy: fix permissions on
    org.apache.tomcat.util.digester package.

 -- Michael Koch <konqueror@gmx.de>  Wed, 03 Oct 2007 20:04:18 +0200

tomcat5.5 (5.5.23-1) unstable; urgency=low

  [ Marcus Better ]
  * New upstream version.
  * Don't include /var/lib/tomcat5.5/conf/catalina.policy since it is
    auto-generated at startup. Thanks to Javier Serrano Polo. (Closes:
    #426761)
  * Make sure files under /var/lib/tomcat5.5/conf are not
    executable. Thanks to Marco Nenciarini. (Closes: #426740)
  * Fixes a failure to start if the temp directory contained too many
    files. Thanks to Maarten van der Hoef (Closes: #427978)
  * tomcat5.5-admin now depends on libcommons-collections3-java and
    libcommons-digester-java.

  [ Michael Koch ]
  * Clean up correctly on (re-)build (Closes: #434617).
  * Replaced obsolete ${Source-Version} by ${source:Version}.
  * Added myself to Uploaders.
  * Updated (Build-)Depends on libcommons-modeler-java to (>= 2.0).
  * Clear up comment in debian/tomcat5.5.default about TOMCAT55_USER.
    (Closes: #425020).
  * Make cron.daily script work when package is removed but not purged
    (Closes: #436020).
  * Applied patch from David Pashley to move configuration from
    /var/lib/tomcat5.5/conf to /etc/tomcat5.5 (Closes: #434728).
  * Use ${catalina.base} instead of ${catalina.home} in
    debian/policy/50user.policy (Closes: #431704).
  * Make tomcat5.5 depend on libecj-java instead of ecj-bootstrap
    (Closes: #396170).
  * Don't make tomcat5.5 on two non-virtual java runtimes. Removed kaffe.
  * Don't let tomcat5.5 suggest libapache-mod-jk. Doesn't exist anymore.
  * Fixed watch file.
  * Set CATALINA_BASE to /var/lib/$NAME in debian/tomcat5.5.init.

 -- Michael Koch <konqueror@gmx.de>  Sun, 02 Sep 2007 11:28:05 +0200

tomcat5.5 (5.5.20-5) unstable; urgency=low

  * Messages are now logged to the system log instead of the pipe
    "/var/log/tomcat5.5/catalina.out". (Closes: #402603, #402879)
  * The security manager is enabled by default. A warning is logged to the
    syslog when running under GCJ, which doesn't work in this mode
    currently. (Closes: #411137)
  * debian/rules: Set SHELL=/bin/bash as required. Thanks to David
    Pashley. (Closes: #413845)
  * Webapps are now permitted to read the properties "catalina.base" and
    "catalina.home". Thanks to Adrian Bridgett. (Closes: #412479)
  * Added the symlink "/usr/share/tomcat5.5/webapps" pointing to
    "/var/lib/tomcat5.5/webapps". Thanks to Cyrille
    <cnofficial@neotilus.com> and Florent Angebault. (Closes: #406614)
  * Build with source level 1.5, since GCJ now supports generics. (Closes:
    #424465)
  * Recognise Sun JDK 6 from the sun-java6-jdk package.
  * debian/control: Removed Stefan Gybas (on own request) and Wolfgang
    Baer from Uploaders.

 -- Marcus Better <marcus@better.se>  Tue, 24 Apr 2007 15:04:17 +0200

tomcat5.5 (5.5.20-4) unstable; urgency=low

  * The symlink "/usr/share/tomcat5.5/common/endorsed/xml-apis" is now
    correctly named "xml-apis.jar". Thanks to Javier Serrano
    Polo. (Closes: #402265)
  * The tomcat55 user now has write permissions to
    "/var/lib/tomcat5.5/conf", so that Tomcat can deploy webapps in WAR
    archives. (Closes: #402224)
  * The named pipe /var/log/tomcat5.5/catalina.out is now properly
    created.
  * The TOMCAT5_SHUTDOWN variable in /etc/default/tomcat5.5 is obsolete
    and has been removed.
  * The init script now supports the try-restart action.
  * Install the cron file.

 -- Marcus Better <marcus@better.se>  Sat,  9 Dec 2006 22:41:52 +0100

tomcat5.5 (5.5.20-3) experimental; urgency=low

  * Really fix the "/etc/init.d/tomcat5.5 status" command this
    time. (Closes: #398044)
  * Use jsvc for daemon startup, instead of the catalina.sh script. Daemon
    shutdown is now faster and more reliable.
  * Add missing permissions required by the admin webapp. Thanks to
    Jonathan-Marc Lapointe.
  * The CATALINA_OPTS variable in /etc/default/tomcat5.5 has been renamed
    to the more intuitive JAVA_OPTS.

 -- Marcus Better <marcus@better.se>  Sat, 25 Nov 2006 21:20:18 +0100

tomcat5.5 (5.5.20-2) unstable; urgency=medium

  * Now runs with java-gcj-compat. (Closes: #395167)
  * Add compatibility links for JDK 1.4. Thanks to Javier Serrano
    Polo. (Closes: #397996).
  * Fix accidental double removal of the tomcat55 user on purge in some
    cases. Thanks to Andreas Beckmann. (Closes: #393224)
  * Fix typo affecting "/etc/init.d/tomcat5.5 status" command. Thanks to
    Jhair Tocancipa Triana. (Closes: #398044)
  * Webapps are now installed in /usr/share/tomcat5.5-webapps. Their
    context definitions are copied into
    /var/lib/tomcat5.5/conf/Catalina/localhost on first install, but if
    removed they will no longer be re-enabled on every upgrade. Thanks to
    Adrian Bridgett. (Closes: #399184)
  * Change owner of various files from tomcat5 to tomcat55 on upgrade from
    release 5.5.17-1 or earlier. Thanks to Mike Whitaker. (Closes: #392569)
  * Don't use juli LogManager with java-gcj-compat, as workaround for bug
    #399251.
  * We no longer need xsltproc to generate documentation, since the
    required supporting functions are now available in main.
  * debian/ant.properties, debian/rules: Don't delete PureTLS code from
    the source. It will be ignored during build anyway.
  * Rebuild jkstatus-ant from source instead of using precompiled class
    files.
  * Simplified the .install files by grouping by directory.
  * Install forgotten files `catalina.properties', `logging.properties'
    and `context.xml' in /var/lib/tomcat5.5/conf.
  * Synchronized security policy files with upstream.
  * debian/rules: Don't use CDBS. Thus we no longer need a bogus "ant xxx"
    invocation in the "clean" target, and the script is simple enough
    anyway.

 -- Marcus Better <marcus@better.se>  Tue, 21 Nov 2006 12:06:17 +0100

tomcat5.5 (5.5.20-1) unstable; urgency=low

  * New upstream release.
  * Build JSP and servlet examples from source instead of copying them
    from libservlet2.4-java. (Closes: #393905)
  * debian/control: Add some missing dependencies.
  * Enable commons-daemon functionality, it should work correctly by now.
  * Make init script LSB compliant.
  * Don't call /etc/init.d/tomcat5.5 directly from the maintainer scripts.

 -- Marcus Better <marcus@better.se>  Mon, 23 Oct 2006 13:28:15 +0200

tomcat5.5 (5.5.17-2) unstable; urgency=low

  * debian/control: removed apache-utils version and added alternative
    apache2-utils (that provide the virtual apache-utils package)
  * debian/tomcat5.init: added Sun's jdk path as provided by sun-java5-bin
    package. (closes: #388617).
  * debian/control: updated apache2-common to apache2.2-common (closes:
    #391006) (thanks to Andrew Meaden and Luk Claes).
  * debian/control (depends): removed -webapps and -admin from Depends
    field and move them to Suggest to avoid circular dependencies, thanks
    to Bill Allombert. (closes: #387362).
  * tomcat5.5 has now the user tomcat55 and remove user tomcat5 if package
    tomcat5 is marked as purge or if it's never been installed (closes:
    #386831).

 -- Arnaud Vandyck <avdyk@debian.org>  Mon,  9 Oct 2006 16:36:25 +0200

tomcat5.5 (5.5.17-1) unstable; urgency=low

  * New upstream release.
  * debian/control: added tomcat5.5 dependency against ecj-boostrap so
    there is a jsp compiler closes: #384062).

 -- Arnaud Vandyck <avdyk@debian.org>  Tue, 12 Sep 2006 14:42:58 +0200

tomcat5.5 (5.5.15-1) experimental; urgency=low

  * Arnaud Vandyck <avdyk@debian.org>:
    + All the work as been done by Wolfgang to have this package in
    Debian.
    + The package is now tomcat5.5 and not tomcat5.
    + Now build with gcj instead of kaffe.
    + Put cdbs and debhelper in Build-Depends.
    + Standards-Version updated to 3.7.2.
    + tomcat depends on tomcat-webapps and tomcat-admin, not only suggest
  * New major upstream release
    + New source layout - adaptions all over the place
    + Ported all patches to new source layout
    + Added patch (09_UseSystemDBCP.patch) to use system dbcp instead of 
      repackaged tomcat stuff (naming-factory-dbcp.jar)
    + Drop now unneeded dependencies on libsaxpath-java, libjaxen-java,
      libregexp-java from build-dependencies and dependencies 
    + Move dependency on libcommons-collections3-java, 
      libcommons-fileupload-java, libcommons-beanutils-java and
      libcommons-digester-java to tomcat5-admin (only needed here)
    + Move libraries around as required by new binary layout (e.g. i18n jars
      into own directory)
    + Moved and linked new jars (tomcat-jkstatus-ant.jar, tomcat-juli.jar)
    + Updated 03catalina.policy to include tomcat-juli.jar, remove launcher.jar
    + Install ant task definitions with libtomcat5-java
  * Remove JDK 1.3 directories from JDK_DIRS in tomcat.init (not supported)
  * Updated tomcat.default to remove JDK 1.3 options
  * Updated description to include host-manager, fixed URLs
  * Minor updates in README.Debian

 -- Wolfgang Baer <WBaer@gmx.de>  Fri, 27 Jan 2006 10:07:47 +0100

tomcat5 (5.0.30-9) unstable; urgency=low

  * kaffe compiler transition
    + Remove build.compiler jikes property
    + Modify tomcat5.init to use default compiler instead of jikes
  * Fix spelling error in README.Debian (closes: #346573)

 -- Wolfgang Baer <WBaer@gmx.de>  Tue, 17 Jan 2006 11:50:52 +0100

tomcat5 (5.0.30-8) unstable; urgency=low

  * Correct description to refer to tomcat5-webapps (closes: #336984)
  * Test in tomcat5.init for JDK like environment so we are sure
    java-gcj-compat-dev is installed and used (closes: #337270)

 -- Wolfgang Baer <WBaer@gmx.de>  Mon,  7 Nov 2005 19:17:58 +0100

tomcat5 (5.0.30-7) unstable; urgency=low

  * Move to main - upload with orig.tar.gz included
  * Works with kaffe (>= 1.1.6-3) and java-gcj-compat-dev
    + changed debian/control accordingly
    + adjusted tomcat5.init file
  * Removed libcommons-httpclient-java build-dep - not needed

 -- Wolfgang Baer <WBaer@gmx.de>  Thu, 06 Oct 2005 21:59:36 +0200

tomcat5 (5.0.30-6) unstable; urgency=low

  * Move commons-fileupload jar from common/lib to server/lib
  * Copy fixed struts-config.xml during install over the old one. The fixed
    one is taken from upstream (published at download location)
  * Tomcat5 has a new context file handling. The xml files with the context of
    a webapp needs to be put into conf/[Engine name]/[Host name] whereas the 
    default engine which serves the admin webapp has to be called "Catalina":
    + Create conf/Catalina/localhost directory during install 
    + Deploy the context files for the webapps into this directory
      (closes: #315038)
    + Added NEWS.Debian file to inform about moving context files to new
      context directory hierarchy.
  * Install the jsp/servlet examples (closes: #325508, #325507, #326126)
  * Added unzip to build-deps (needed to unzip example webapps)
  * Cleaned up debian/rules a bit
  * libcommons-launcher-java dependency added in last upload (closes: #324041)
  * Works with latest kaffe vm (closes: #320845) - added versioned dependency
  * Changed depends to j2sdk1.3 | j2sdk1.4 | j2sdk1.5 | kaffe
  * Removed depends on j2sdk1.3 | j2sdk1.4 | j2sdk1.5 | kaffe | java-compiler 
    as the above depends already includes an java-compiler as JDKs
  * Removed note in description about non-free JDKs - kaffe works and the
    info is superceded by the existance of java-package
  * Updated *.policy files to match new jars and fixed typos
  * As tomcat5 is now functional we can close the whishlist bugs for
    packaging tomcat5 (closes: #222876, #267741)
  * Disable commons-daemon copy/build - not available on all platforms
    + Added patch 15_dont_copy_daemon.patch
    + Removed related stuff from debian/rules and debian/control   
  * Removed classpath entry from MF and added jars directly to setclasspath.sh
    + Patch 01_remove_classpath_manifest_a.patch removes the entry
    + Patch 01_remove_classpath_manifest_b.patch adds it to setclasspath.sh
  * Fixed typo CALALINA in README.Debian
  * Fixed lintian warning in tomcat5.postinst
  * Extended JDK_DIRS and enabled Security Manager for non-free JDKs
  * Build documentation through commandline xslt processing
    + Patch 03_fix_doc_stylesheet.patch to fix stylsheet variable definition
    + Added xsltproc to build-deps and process docs in debian/rules
    + Moved RELEASE-NOTES to webapps/ROOT where it belongs
    + Added tomcat-docs.xml context definition to allow linking and also
      starting tomcat5 without tomcat5-webapps installed (removed linking
      for tomcat-docs from server.xml)
  * Added myself to uploaders

  * Upload sponsored by Petter Reinholdtsen

 -- Wolfgang Baer <WBaer@gmx.de>  Wed, 07 Sep 2005 17:25:44 +0200

tomcat5 (5.0.30-5) unstable; urgency=low

  * debian/control (libtomcat5-java: Depends): added
    libcommons-launcher-java

 -- Arnaud Vandyck <avdyk@debian.org>  Sat, 20 Aug 2005 10:34:05 +0200

tomcat5 (5.0.30-4) unstable; urgency=low

  * upload to unstable
  * debian/control: ant transition: change the dep from libant1.6-java to
    ant
  * debian/libtomcat5-java.links: added some links and corrected ant once
  * updated to libcommons-collection3-java

 -- Arnaud Vandyck <avdyk@debian.org>  Fri, 19 Aug 2005 16:06:27 +0200

tomcat5 (5.0.30-3) experimental; urgency=low

  * Added libapache2-mod-jk to suggests, missing libcommons-el-java
    to libtomcat5-java build-depends
  * Fixed/Added links in libtomcat5-java (jsp-api/commons-el jars added)
  * Changed JDK dirs (as generated by java-package), enabled security
    manager and therefore put kaffe as last jdk (currently has problems) 
  * Fixed typo in ant.properties to correctly include logging-api.jar
    and fixed libtomcat5-java accordingly   
  * Fixed servlet-api link in tomcat5-webapps.links and included needed
    jsp-api link
  * Standards-Version 3.6.2 - no changes required
  * Build-Deps to Build-Dep-Indep to fix linitan warning

 -- Wolfgang Baer <WBaer@gmx.de>  Wed, 29 Jun 2005 22:32:49 +0200

tomcat5 (5.0.30-2) experimental; urgency=low

  * libtomcat5-java: does not provide commons-logging-api and
    commons-daemon but use symlink to /usr/share/java. Now, tomcat5 can
    start with non-free jdks (but it does not run with free one at the
    moment for me) (closes: #315038)

 -- Arnaud Vandyck <avdyk@debian.org>  Tue, 21 Jun 2005 12:44:21 +0200

tomcat5 (5.0.30-1) experimental; urgency=low

  * Initial release release based on tomcat4 package by Stefan Gybas.

 -- Arnaud Vandyck <avdyk@debian.org>  Sat, 11 Jun 2005 17:00:35 +0200
tomcat5.5 (5.5.26-2) unstable; urgency=low

  * Fixed bootstrap.MF to put commons-logging-api.jar onto classpath instead
    of commons-logging-api-1.1.1.jar. Closes: #477363

 -- Michael Koch <konqueror@gmx.de>  Sun, 01 Jun 2008 11:34:40 +0200

tomcat5.5 (5.5.26-1) unstable; urgency=low

  [ Michael Koch ]
  * New upstream release.
    - CVE-2007-5333: unauthorized disclosure of information. Closes: #465645
    - CVS-2007-6286: handling of empty requests.
  * debian/rules: Don't file when files to delete don't exist.
    Closes: #458977
  * debian/tomcat5.5.init: Change directory to $CATALINA_BASE/temp before
    starting the daemon. Patch by David Pashley. Closes: #418826
  * debian/tomcat5.5.init: Use 'printf' instead of 'echo -e'.
    Closes: #472899

  [ Marcus Better ]
  * debian/policy/04webapps.policy: Grant read permission to JULI for the
    (non-existing) logging.properties file in the example webapps. Closes:
    #460839.

 -- Michael Koch <konqueror@gmx.de>  Sat, 19 Apr 2008 23:18:30 +0200

tomcat5.5 (5.5.25-5) unstable; urgency=low

  * debian/tomcat5.5.init: Check if tomcat-users.xml exists.
    Thanks to Javier Serrano Polo for the patch. Closes: #445857.
  * debian/tomcat5.5-webapps.postrm: Purge links created in postinst script.
    Closes: #453879.
  * debian/tomcat5.5-admin.links: Fix symlink for commons-io.jar.
    Closes: #452366.
  * debian/tomcat5.5.init: Check user id of the user running the init script.
    Closes: #457956.
  * Renamed /etc/cron.daily/tomcat5.5 to /etc/cron.daily/tomcat55.
    Closes: #454296.
  * debian/tomcat5.5.init: source /etc/default/locale and export LANG so
    tomcat gets started with system locale. Originally reported to
    https://bugs.launchpad.net/ubuntu/+source/tomcat5.5/+bug/153672.

 -- Michael Koch <konqueror@gmx.de>  Thu, 03 Jan 2008 13:23:44 +0100

tomcat5.5 (5.5.25-4) unstable; urgency=high

  * CVE-2007-5342: Fix unauthorized modification of data because of
    too open permissions. Closes: #458237.
  * Always clean temporary directory on startup. Closes: #456608.

 -- Michael Koch <konqueror@gmx.de>  Sat, 29 Dec 2007 20:15:40 +0100

tomcat5.5 (5.5.25-3) unstable; urgency=low

  * debian/libtomcat5.5-java.links: Removed links for xml-apis.jar and
    xercesImpl.jar. Closes: #443382, #455495.
  * Added libgnumail-java to Build-Depends. Closes: #454312.
  * Updated Standards-Version to 3.7.3.

 -- Michael Koch <konqueror@gmx.de>  Thu, 13 Dec 2007 22:15:18 +0100

tomcat5.5 (5.5.25-2) unstable; urgency=high

  [ Michael Koch ]
  CVE-2007-5461:
  * Fix absolute path traversal vulnerability. Closes: #448664.

  [ Marcus Better ]
  * Add required commons-io symlink to the admin webapp, which fixes WAR
    file uploads. (Closes: #452366)
  * debian/control: Use the new Homepage and Vcs-* fields.
  * debian/NEWS: Remove outdated entry.

 -- Michael Koch <konqueror@gmx.de>  Fri, 30 Nov 2007 10:46:33 +0100

tomcat5.5 (5.5.25-1) unstable; urgency=high

  * New upstream release. Setting usrgency to high.
    - Fixes XSS issues. CVE-2007-1355, CVS-2007-2449 and CVE-2007-2450.
  * debian/policy/04webapps.policy: fix permissions on
    org.apache.tomcat.util.digester package.

 -- Michael Koch <konqueror@gmx.de>  Wed, 03 Oct 2007 20:04:18 +0200

tomcat5.5 (5.5.23-1) unstable; urgency=low

  [ Marcus Better ]
  * New upstream version.
  * Don't include /var/lib/tomcat5.5/conf/catalina.policy since it is
    auto-generated at startup. Thanks to Javier Serrano Polo. (Closes:
    #426761)
  * Make sure files under /var/lib/tomcat5.5/conf are not
    executable. Thanks to Marco Nenciarini. (Closes: #426740)
  * Fixes a failure to start if the temp directory contained too many
    files. Thanks to Maarten van der Hoef (Closes: #427978)
  * tomcat5.5-admin now depends on libcommons-collections3-java and
    libcommons-digester-java.

  [ Michael Koch ]
  * Clean up correctly on (re-)build (Closes: #434617).
  * Replaced obsolete ${Source-Version} by ${source:Version}.
  * Added myself to Uploaders.
  * Updated (Build-)Depends on libcommons-modeler-java to (>= 2.0).
  * Clear up comment in debian/tomcat5.5.default about TOMCAT55_USER.
    (Closes: #425020).
  * Make cron.daily script work when package is removed but not purged
    (Closes: #436020).
  * Applied patch from David Pashley to move configuration from
    /var/lib/tomcat5.5/conf to /etc/tomcat5.5 (Closes: #434728).
  * Use ${catalina.base} instead of ${catalina.home} in
    debian/policy/50user.policy (Closes: #431704).
  * Make tomcat5.5 depend on libecj-java instead of ecj-bootstrap
    (Closes: #396170).
  * Don't make tomcat5.5 on two non-virtual java runtimes. Removed kaffe.
  * Don't let tomcat5.5 suggest libapache-mod-jk. Doesn't exist anymore.
  * Fixed watch file.
  * Set CATALINA_BASE to /var/lib/$NAME in debian/tomcat5.5.init.

 -- Michael Koch <konqueror@gmx.de>  Sun, 02 Sep 2007 11:28:05 +0200

tomcat5.5 (5.5.20-5) unstable; urgency=low

  * Messages are now logged to the system log instead of the pipe
    "/var/log/tomcat5.5/catalina.out". (Closes: #402603, #402879)
  * The security manager is enabled by default. A warning is logged to the
    syslog when running under GCJ, which doesn't work in this mode
    currently. (Closes: #411137)
  * debian/rules: Set SHELL=/bin/bash as required. Thanks to David
    Pashley. (Closes: #413845)
  * Webapps are now permitted to read the properties "catalina.base" and
    "catalina.home". Thanks to Adrian Bridgett. (Closes: #412479)
  * Added the symlink "/usr/share/tomcat5.5/webapps" pointing to
    "/var/lib/tomcat5.5/webapps". Thanks to Cyrille
    <cnofficial@neotilus.com> and Florent Angebault. (Closes: #406614)
  * Build with source level 1.5, since GCJ now supports generics. (Closes:
    #424465)
  * Recognise Sun JDK 6 from the sun-java6-jdk package.
  * debian/control: Removed Stefan Gybas (on own request) and Wolfgang
    Baer from Uploaders.

 -- Marcus Better <marcus@better.se>  Tue, 24 Apr 2007 15:04:17 +0200

tomcat5.5 (5.5.20-4) unstable; urgency=low

  * The symlink "/usr/share/tomcat5.5/common/endorsed/xml-apis" is now
    correctly named "xml-apis.jar". Thanks to Javier Serrano
    Polo. (Closes: #402265)
  * The tomcat55 user now has write permissions to
    "/var/lib/tomcat5.5/conf", so that Tomcat can deploy webapps in WAR
    archives. (Closes: #402224)
  * The named pipe /var/log/tomcat5.5/catalina.out is now properly
    created.
  * The TOMCAT5_SHUTDOWN variable in /etc/default/tomcat5.5 is obsolete
    and has been removed.
  * The init script now supports the try-restart action.
  * Install the cron file.

 -- Marcus Better <marcus@better.se>  Sat,  9 Dec 2006 22:41:52 +0100

tomcat5.5 (5.5.20-3) experimental; urgency=low

  * Really fix the "/etc/init.d/tomcat5.5 status" command this
    time. (Closes: #398044)
  * Use jsvc for daemon startup, instead of the catalina.sh script. Daemon
    shutdown is now faster and more reliable.
  * Add missing permissions required by the admin webapp. Thanks to
    Jonathan-Marc Lapointe.
  * The CATALINA_OPTS variable in /etc/default/tomcat5.5 has been renamed
    to the more intuitive JAVA_OPTS.

 -- Marcus Better <marcus@better.se>  Sat, 25 Nov 2006 21:20:18 +0100

tomcat5.5 (5.5.20-2) unstable; urgency=medium

  * Now runs with java-gcj-compat. (Closes: #395167)
  * Add compatibility links for JDK 1.4. Thanks to Javier Serrano
    Polo. (Closes: #397996).
  * Fix accidental double removal of the tomcat55 user on purge in some
    cases. Thanks to Andreas Beckmann. (Closes: #393224)
  * Fix typo affecting "/etc/init.d/tomcat5.5 status" command. Thanks to
    Jhair Tocancipa Triana. (Closes: #398044)
  * Webapps are now installed in /usr/share/tomcat5.5-webapps. Their
    context definitions are copied into
    /var/lib/tomcat5.5/conf/Catalina/localhost on first install, but if
    removed they will no longer be re-enabled on every upgrade. Thanks to
    Adrian Bridgett. (Closes: #399184)
  * Change owner of various files from tomcat5 to tomcat55 on upgrade from
    release 5.5.17-1 or earlier. Thanks to Mike Whitaker. (Closes: #392569)
  * Don't use juli LogManager with java-gcj-compat, as workaround for bug
    #399251.
  * We no longer need xsltproc to generate documentation, since the
    required supporting functions are now available in main.
  * debian/ant.properties, debian/rules: Don't delete PureTLS code from
    the source. It will be ignored during build anyway.
  * Rebuild jkstatus-ant from source instead of using precompiled class
    files.
  * Simplified the .install files by grouping by directory.
  * Install forgotten files `catalina.properties', `logging.properties'
    and `context.xml' in /var/lib/tomcat5.5/conf.
  * Synchronized security policy files with upstream.
  * debian/rules: Don't use CDBS. Thus we no longer need a bogus "ant xxx"
    invocation in the "clean" target, and the script is simple enough
    anyway.

 -- Marcus Better <marcus@better.se>  Tue, 21 Nov 2006 12:06:17 +0100

tomcat5.5 (5.5.20-1) unstable; urgency=low

  * New upstream release.
  * Build JSP and servlet examples from source instead of copying them
    from libservlet2.4-java. (Closes: #393905)
  * debian/control: Add some missing dependencies.
  * Enable commons-daemon functionality, it should work correctly by now.
  * Make init script LSB compliant.
  * Don't call /etc/init.d/tomcat5.5 directly from the maintainer scripts.

 -- Marcus Better <marcus@better.se>  Mon, 23 Oct 2006 13:28:15 +0200

tomcat5.5 (5.5.17-2) unstable; urgency=low

  * debian/control: removed apache-utils version and added alternative
    apache2-utils (that provide the virtual apache-utils package)
  * debian/tomcat5.init: added Sun's jdk path as provided by sun-java5-bin
    package. (closes: #388617).
  * debian/control: updated apache2-common to apache2.2-common (closes:
    #391006) (thanks to Andrew Meaden and Luk Claes).
  * debian/control (depends): removed -webapps and -admin from Depends
    field and move them to Suggest to avoid circular dependencies, thanks
    to Bill Allombert. (closes: #387362).
  * tomcat5.5 has now the user tomcat55 and remove user tomcat5 if package
    tomcat5 is marked as purge or if it's never been installed (closes:
    #386831).

 -- Arnaud Vandyck <avdyk@debian.org>  Mon,  9 Oct 2006 16:36:25 +0200

tomcat5.5 (5.5.17-1) unstable; urgency=low

  * New upstream release.
  * debian/control: added tomcat5.5 dependency against ecj-boostrap so
    there is a jsp compiler closes: #384062).

 -- Arnaud Vandyck <avdyk@debian.org>  Tue, 12 Sep 2006 14:42:58 +0200

tomcat5.5 (5.5.15-1) experimental; urgency=low

  * Arnaud Vandyck <avdyk@debian.org>:
    + All the work as been done by Wolfgang to have this package in
    Debian.
    + The package is now tomcat5.5 and not tomcat5.
    + Now build with gcj instead of kaffe.
    + Put cdbs and debhelper in Build-Depends.
    + Standards-Version updated to 3.7.2.
    + tomcat depends on tomcat-webapps and tomcat-admin, not only suggest
  * New major upstream release
    + New source layout - adaptions all over the place
    + Ported all patches to new source layout
    + Added patch (09_UseSystemDBCP.patch) to use system dbcp instead of 
      repackaged tomcat stuff (naming-factory-dbcp.jar)
    + Drop now unneeded dependencies on libsaxpath-java, libjaxen-java,
      libregexp-java from build-dependencies and dependencies 
    + Move dependency on libcommons-collections3-java, 
      libcommons-fileupload-java, libcommons-beanutils-java and
      libcommons-digester-java to tomcat5-admin (only needed here)
    + Move libraries around as required by new binary layout (e.g. i18n jars
      into own directory)
    + Moved and linked new jars (tomcat-jkstatus-ant.jar, tomcat-juli.jar)
    + Updated 03catalina.policy to include tomcat-juli.jar, remove launcher.jar
    + Install ant task definitions with libtomcat5-java
  * Remove JDK 1.3 directories from JDK_DIRS in tomcat.init (not supported)
  * Updated tomcat.default to remove JDK 1.3 options
  * Updated description to include host-manager, fixed URLs
  * Minor updates in README.Debian

 -- Wolfgang Baer <WBaer@gmx.de>  Fri, 27 Jan 2006 10:07:47 +0100

tomcat5 (5.0.30-9) unstable; urgency=low

  * kaffe compiler transition
    + Remove build.compiler jikes property
    + Modify tomcat5.init to use default compiler instead of jikes
  * Fix spelling error in README.Debian (closes: #346573)

 -- Wolfgang Baer <WBaer@gmx.de>  Tue, 17 Jan 2006 11:50:52 +0100

tomcat5 (5.0.30-8) unstable; urgency=low

  * Correct description to refer to tomcat5-webapps (closes: #336984)
  * Test in tomcat5.init for JDK like environment so we are sure
    java-gcj-compat-dev is installed and used (closes: #337270)

 -- Wolfgang Baer <WBaer@gmx.de>  Mon,  7 Nov 2005 19:17:58 +0100

tomcat5 (5.0.30-7) unstable; urgency=low

  * Move to main - upload with orig.tar.gz included
  * Works with kaffe (>= 1.1.6-3) and java-gcj-compat-dev
    + changed debian/control accordingly
    + adjusted tomcat5.init file
  * Removed libcommons-httpclient-java build-dep - not needed

 -- Wolfgang Baer <WBaer@gmx.de>  Thu, 06 Oct 2005 21:59:36 +0200

tomcat5 (5.0.30-6) unstable; urgency=low

  * Move commons-fileupload jar from common/lib to server/lib
  * Copy fixed struts-config.xml during install over the old one. The fixed
    one is taken from upstream (published at download location)
  * Tomcat5 has a new context file handling. The xml files with the context of
    a webapp needs to be put into conf/[Engine name]/[Host name] whereas the 
    default engine which serves the admin webapp has to be called "Catalina":
    + Create conf/Catalina/localhost directory during install 
    + Deploy the context files for the webapps into this directory
      (closes: #315038)
    + Added NEWS.Debian file to inform about moving context files to new
      context directory hierarchy.
  * Install the jsp/servlet examples (closes: #325508, #325507, #326126)
  * Added unzip to build-deps (needed to unzip example webapps)
  * Cleaned up debian/rules a bit
  * libcommons-launcher-java dependency added in last upload (closes: #324041)
  * Works with latest kaffe vm (closes: #320845) - added versioned dependency
  * Changed depends to j2sdk1.3 | j2sdk1.4 | j2sdk1.5 | kaffe
  * Removed depends on j2sdk1.3 | j2sdk1.4 | j2sdk1.5 | kaffe | java-compiler 
    as the above depends already includes an java-compiler as JDKs
  * Removed note in description about non-free JDKs - kaffe works and the
    info is superceded by the existance of java-package
  * Updated *.policy files to match new jars and fixed typos
  * As tomcat5 is now functional we can close the whishlist bugs for
    packaging tomcat5 (closes: #222876, #267741)
  * Disable commons-daemon copy/build - not available on all platforms
    + Added patch 15_dont_copy_daemon.patch
    + Removed related stuff from debian/rules and debian/control   
  * Removed classpath entry from MF and added jars directly to setclasspath.sh
    + Patch 01_remove_classpath_manifest_a.patch removes the entry
    + Patch 01_remove_classpath_manifest_b.patch adds it to setclasspath.sh
  * Fixed typo CALALINA in README.Debian
  * Fixed lintian warning in tomcat5.postinst
  * Extended JDK_DIRS and enabled Security Manager for non-free JDKs
  * Build documentation through commandline xslt processing
    + Patch 03_fix_doc_stylesheet.patch to fix stylsheet variable definition
    + Added xsltproc to build-deps and process docs in debian/rules
    + Moved RELEASE-NOTES to webapps/ROOT where it belongs
    + Added tomcat-docs.xml context definition to allow linking and also
      starting tomcat5 without tomcat5-webapps installed (removed linking
      for tomcat-docs from server.xml)
  * Added myself to uploaders

  * Upload sponsored by Petter Reinholdtsen

 -- Wolfgang Baer <WBaer@gmx.de>  Wed, 07 Sep 2005 17:25:44 +0200

tomcat5 (5.0.30-5) unstable; urgency=low

  * debian/control (libtomcat5-java: Depends): added
    libcommons-launcher-java

 -- Arnaud Vandyck <avdyk@debian.org>  Sat, 20 Aug 2005 10:34:05 +0200

tomcat5 (5.0.30-4) unstable; urgency=low

  * upload to unstable
  * debian/control: ant transition: change the dep from libant1.6-java to
    ant
  * debian/libtomcat5-java.links: added some links and corrected ant once
  * updated to libcommons-collection3-java

 -- Arnaud Vandyck <avdyk@debian.org>  Fri, 19 Aug 2005 16:06:27 +0200

tomcat5 (5.0.30-3) experimental; urgency=low

  * Added libapache2-mod-jk to suggests, missing libcommons-el-java
    to libtomcat5-java build-depends
  * Fixed/Added links in libtomcat5-java (jsp-api/commons-el jars added)
  * Changed JDK dirs (as generated by java-package), enabled security
    manager and therefore put kaffe as last jdk (currently has problems) 
  * Fixed typo in ant.properties to correctly include logging-api.jar
    and fixed libtomcat5-java accordingly   
  * Fixed servlet-api link in tomcat5-webapps.links and included needed
    jsp-api link
  * Standards-Version 3.6.2 - no changes required
  * Build-Deps to Build-Dep-Indep to fix linitan warning

 -- Wolfgang Baer <WBaer@gmx.de>  Wed, 29 Jun 2005 22:32:49 +0200

tomcat5 (5.0.30-2) experimental; urgency=low

  * libtomcat5-java: does not provide commons-logging-api and
    commons-daemon but use symlink to /usr/share/java. Now, tomcat5 can
    start with non-free jdks (but it does not run with free one at the
    moment for me) (closes: #315038)

 -- Arnaud Vandyck <avdyk@debian.org>  Tue, 21 Jun 2005 12:44:21 +0200

tomcat5 (5.0.30-1) experimental; urgency=low

  * Initial release release based on tomcat4 package by Stefan Gybas.

 -- Arnaud Vandyck <avdyk@debian.org>  Sat, 11 Jun 2005 17:00:35 +0200
